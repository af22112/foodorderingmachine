import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrderingMachine {
    private JPanel root;
    private JLabel topLabel;
    private JButton SoysauceButton;
    private JButton MisoButton;
    private JButton TonkotuButton;
    private JButton FriedRiceButton;
    private JButton RiceButton;
    private JButton GyozaButton;
    private JLabel orderItemsLabel;
    private JTextPane orderItemList;
    private JButton checkOutButton;
    private JLabel totalPriceLabel;
    String orderedItems = "";
    int totalPrice = 0;

    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrderingMachine");
        frame.setContentPane(new FoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    void order(String food, int price) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "!");
            orderedItems += food + " " + price + "yen\n";
            orderItemList.setText(orderedItems);
            totalPrice += price;
            totalPriceLabel.setText("Total " + totalPrice + " yen  ");
        }
    }

    void orderBig(String food, int price) {
        int confirmation1 = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation1 == 0) {
            int confirmation2 = JOptionPane.showConfirmDialog(
                    null,
                    "+100 yen and Change big size?",
                    "Big Size",
                    JOptionPane.YES_NO_OPTION);
            if (confirmation2 == 0) {
                price += 100;
                food += "(Big size)";
                JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "!");
                orderedItems += food + " " + price + "yen\n";
                orderItemList.setText(orderedItems);
                totalPrice += price;
                totalPriceLabel.setText("Total " + totalPrice + " yen  ");
            } else {
                JOptionPane.showMessageDialog(null, "Thank you for ordering " + food + "!");
                orderedItems += food + " " + price + "yen\n";
                orderItemList.setText(orderedItems);
                totalPrice += price;
                totalPriceLabel.setText("Total " + totalPrice + " yen  ");
            }
        }
    }

    public FoodOrderingMachine() {
        SoysauceButton.setIcon(new ImageIcon(
                this.getClass().getResource("/Soysauce.jpg")
        ));
        MisoButton.setIcon(new ImageIcon(
                this.getClass().getResource("/Miso.jpg")
        ));
        TonkotuButton.setIcon(new ImageIcon(
                this.getClass().getResource("/Tonkotu.jpg")
        ));
        FriedRiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("/FriedRice.jpg")
        ));
        RiceButton.setIcon(new ImageIcon(
                this.getClass().getResource("/Rice.jpg")
        ));
        GyozaButton.setIcon(new ImageIcon(
                this.getClass().getResource("/Gyoza.jpg")
        ));

        SoysauceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderBig("Ramen(Soy sauce)", 600);

            }
        });
        MisoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderBig("Ramen(Miso)", 640);
            }
        });
        TonkotuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderBig("Ramen(Tonkotu)", 680);
            }
        });
        FriedRiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderBig("Fried rice", 500);
            }
        });
        RiceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderBig("Rice", 300);
            }
        });
        GyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza", 400);
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " + totalPrice + " yen.");
                    orderedItems = "";
                    totalPrice = 0;
                    orderItemList.setText(orderedItems);
                    totalPriceLabel.setText("Total " + totalPrice + " yen  ");
                }
            }
        });

    }
}